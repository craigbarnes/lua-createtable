CC ?= gcc
CFLAGS ?= -g -O2
XCFLAGS += -std=c99 -pedantic-errors -fpic -fvisibility=hidden
CWARNS = -Wall -Wextra -Wshadow -Wundef -Wconversion -Wc90-c99-compat
XLDFLAGS = -shared
MAKEFLAGS += -r
LUA = lua
GIT = git
RELEASE_VERSIONS = 0.2 0.1
RELEASE_DIST = $(foreach V, $(RELEASE_VERSIONS), lua-createtable-$(V).tar.gz)
DISTVER = $(firstword $(RELEASE_VERSIONS))

default:
	@echo 'This Makefile is for development purposes only.' >&2
	@echo 'Use "luarocks make" to build and install.' >&2

all: createtable.so
dist: lua-createtable-$(DISTVER).tar.gz

createtable.so: createtable.o
	$(CC) $(CFLAGS) $(LDFLAGS) $(XLDFLAGS) -o $@ $^

createtable.o: createtable.c compat.h
	$(CC) $(CPPFLAGS) $(CFLAGS) $(XCFLAGS) $(CWARNS) -c -o $@ $<

check: all
	$(LUA) test.lua

$(RELEASE_DIST): lua-createtable-%.tar.gz:
	$(GIT) archive --prefix='lua-createtable-$*/' -o '$@' 'v$*'

clean:
	$(RM) createtable.so createtable.o


.PHONY: default all dist check clean
.DELETE_ON_ERROR:
