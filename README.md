lua-createtable
===============

Lua bindings for the [`lua_createtable()`] function.

This function is useful for preallocating tables with a given number
of array and hash slots, without paying the overheads of incremental
growth and rehashing. This can sometimes improve performance when the
number of slots is large and known ahead of time.

Requirements
------------

* C compiler
* [Lua] `>= 5.1`

Installation
------------

    luarocks install createtable

Example
-------

```lua
local createtable = require "createtable"
local narr, nrec = 512, 16
local t = createtable(narr, nrec)
assert(type(t) == "table")
```


[Lua]: https://www.lua.org/
[`lua_createtable()`]: https://www.lua.org/manual/5.3/manual.html#lua_createtable
