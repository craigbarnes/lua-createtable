/*
 Lua bindings for the lua_createtable() function.
 Copyright (c) 2020, Craig Barnes.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

    https://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

#include <limits.h>
#include <lua.h>
#include <lauxlib.h>
#include "compat.h"

static int createtable(lua_State *L)
{
    lua_Integer narr = luaL_checkinteger(L, 1);
    lua_Integer nrec = luaL_checkinteger(L, 2);
    if (unlikely(narr < 0 || narr > INT_MAX)) {
        luaL_argerror(L, 1, "value outside valid range");
    }
    if (unlikely(nrec < 0 || nrec > INT_MAX)) {
        luaL_argerror(L, 2, "value outside valid range");
    }
    lua_createtable(L, (int)narr, (int)nrec);
    return 1;
}

EXPORT int luaopen_createtable(lua_State *L)
{
    lua_pushcfunction(L, createtable);
    return 1;
}
