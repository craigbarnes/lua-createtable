package = "createtable"
version = "scm-1"

description = {
    summary = "Bindings for the lua_createtable() function",
    homepage = "https://gitlab.com/craigbarnes/lua-createtable",
    license = "Apache-2.0"
}

source = {
    url = "git+https://gitlab.com/craigbarnes/lua-createtable.git",
    branch = "master"
}

dependencies = {
    "lua >= 5.1"
}

build = {
    type = "builtin",
    modules = {
        createtable = {
            sources = {"createtable.c"}
        }
    }
}
